<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBacklogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backlogs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('user_field');
            $table->string('funcional_field');
            $table->string('profit_field');
            $table->string('description');
            $table->string('acceptance_requirements');
            $table->string('responsable');
            $table->string('score');
            $table->integer('sprint');
            $table->unsignedBigInteger('project_id')->unsigned();
            $table->timestamps();
            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backlogs');
    }
}
