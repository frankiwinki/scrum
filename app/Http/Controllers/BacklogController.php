<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Backlog;
use App\Project;

class BacklogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $backlog = Backlog::with(['projects'])->get();
        return view('backlog.index', compact('backlog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $projects = Project::all();
        $backlog = new Backlog();
        return view('backlog.create', compact('backlog', 'edit', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name'=>'required',
                'user_field'=>'required',
                'funcional_field'=>'required',
                'profit_field'=>'required',
                'description'=>'required',
                'acceptance_requirements'=>'required',
                'responsable'=>'required',
                'score'=>'required',
                'sprint'=>'required',
                'project_id'=>'required',
            ]
        );
        $backlog = new Backlog();     
        $backlog->name=$request->name;   
        $backlog->user_field=$request->user_field;
        $backlog->funcional_field=$request->funcional_field;
        $backlog->profit_field=$request->profit_field;
        $backlog->description=$request->description;
        $backlog->acceptance_requirements=$request->acceptance_requirements;
        $backlog->responsable=$request->responsable;
        $backlog->score=$request->score;
        $backlog->sprint=$request->sprint;
        $backlog->project_id=$request->project_id;
        $backlog->save();
        return redirect('backlog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $projects = Project::all();
        $backlog =  Backlog::find($id);
        return view('backlog.create', compact('backlog', 'edit', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name'=>'required',
                'user_field'=>'required',
                'funcional_field'=>'required',
                'profit_field'=>'required',
                'description'=>'required',
                'acceptance_requirements'=>'required',
                'responsable'=>'required',
                'score'=>'required',
                'sprint'=>'required',
                'project_id'=>'required',
            ]
        );
        $backlog=Backlog::find($id);
        $backlog->name=$request->name;
        $backlog->user_field=$request->user_field;
        $backlog->funcional_field=$request->funcional_field;
        $backlog->profit_field=$request->profit_field;
        $backlog->description=$request->description;
        $backlog->acceptance_requirements=$request->acceptance_requirements;
        $backlog->responsable=$request->responsable;
        $backlog->score=$request->score;
        $backlog->sprint=$request->sprint;
        $backlog->project_id=$request->project_id;
        $backlog->save();
        return redirect('backlog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $backlog=Backlog::find($id);
        $backlog->delete();
        return redirect('backlog');
    }
}
