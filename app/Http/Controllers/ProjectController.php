<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects=Project::all();
        return view ('project.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit= false;
        $project = new Project();
        return view ('project.create',compact('project','edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            ['name'=>'required',
            'product_owner'=>'required',
            'scrum_master'=>'required',
            'development_team'=>'required',
            ]
        );
        $project = new Project();
        $project->name=$request->name;
        $project->product_owner=$request->product_owner;
        $project->scrum_master=$request->scrum_master;
        $project->development_team=$request->development_team;
        $project->save();
        return redirect('projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit= true;
        $project =  Project::find($id);
        return view ('project.create',compact('project','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            ['name'=>'required',
            'product_owner'=>'required',
            'scrum_master'=>'required',
            'development_team'=>'required',
            ]
        );
        $project = Project::find($id);
        $project->name=$request->name;
        $project->product_owner=$request->product_owner;
        $project->scrum_master=$request->scrum_master;
        $project->development_team=$request->development_team;
        $project->save();
        return redirect('projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project=Project::find($id);
        $project->delete();
        return redirect('projects');
    }
}
