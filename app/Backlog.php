<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backlog extends Model
{
    public function projects()
    {
        return $this->belongsTo(Project::class, 'project_id')
            ->select('id', 'name');
    }
}
