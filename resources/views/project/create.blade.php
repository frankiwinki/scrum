@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('aside')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Crear Proyecto') }} <a href="{{route('projects.index')}}" class="btn btn-sm btn-success float-right">Listado de proyectos</a></div>
                @if(!$edit)
                <form action="{{route('projects.store')}}" method="POST">
                    @else
                    <form action="{{route('projects.update',['project' => $project->id])}}" method="POST">
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        @csrf
                        <div class="card-body">


                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" name="name" value="{{$project->name}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Product Owner</label>
                                <input type="text" name="product_owner" value="{{$project->product_owner}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Scrum Master</label>
                                <input type="text" name="scrum_master" value="{{$project->scrum_master}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Equipo de Desarrollo</label>
                                <input type="text" name="development_team" value="{{$project->development_team}}" class="form-control">
                            </div>


                        </div>
                        <div class="card-footer bg-white">
                            <button class="mx-2 float-right btn btn-sm btn-primary">Guardar</button>
                            <a href="{{route('home')}}" class="mx-2 float-right btn btn-sm btn-secondary">Cancelar</a>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection