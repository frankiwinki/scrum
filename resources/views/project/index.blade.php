@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('aside')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Listado Proyectos') }} <a href="{{route('projects.create')}}" class="btn btn-sm btn-success float-right">Crear Proyecto</a></div>

                <div class="card-body">

                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Proyecto</th>
                                <th>Product Owner</th>
                                <th>Scrum Master</th>
                                <th>Eliminar</th>
                                <th>Modificar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($projects as $item)
                            <tr>
                                <td>{{$item->name}}</td>
                                <td>{{$item->product_owner}}</td>
                                <td>{{$item->scrum_master}}</td>
                                <td>
                                    
                                    <form action="{{route('projects.destroy',['project'=>$item->id])}}" method="post">
                                        <input class="btn btn-sm btn-danger" type="submit" value="Delete" />
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </td>
                                <td><a href="{{route('projects.edit',['project'=>$item->id])}}" class="btn btn-sm btn-primary">Modificar</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="card-footer">
                    <a href="{{route('home')}}" class="float-right btn btn-sm btn-secondary">Atrás</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection