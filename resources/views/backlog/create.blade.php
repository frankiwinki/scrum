@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('aside')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Crear Historia de Usuario') }} <a href="{{route('backlog.index')}}" class="btn btn-sm btn-success float-right">Listado de Backlog</a></div>
                @if(!$edit)
                <form action="{{route('backlog.store')}}" method="POST">
                    @else
                    <form action="{{route('backlog.update',['backlog' => $backlog->id])}}" method="POST">
                        <input name="_method" type="hidden" value="PUT">
                        @endif
                        @csrf
                        <div class="card-body">

                            <div class="form-group">
                                <label for="">Proyecto</label>
                                <select name="project_id" id="" class="form-control">
                                    @foreach($projects as $row)
                                    <option @if($backlog->project_id==$row->id) selected @endif value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" name="name" value="{{$backlog->name}}" class="form-control">
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="">Campo de usuario</label>
                                    <input type="text" name="user_field" value="{{$backlog->user_field}}" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Campo de funcionalidad</label>
                                    <input type="text" name="funcional_field" value="{{$backlog->funcional_field}}" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Campo de beneficio</label>
                                    <input type="text" name="profit_field" value="{{$backlog->profit_field}}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Descripción</label>
                                <input type="text" name="description" value="{{$backlog->description}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Criterios de aceptación</label>
                                <input type="text" name="acceptance_requirements" value="{{$backlog->acceptance_requirements}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Responsable</label>
                                <input type="text" name="responsable" value="{{$backlog->responsable}}" class="form-control">
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="">Puntuación</label>
                                    <input type="text" name="score" value="{{$backlog->score}}" class="form-control ">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Sprint</label>
                                    <input type="number" name="sprint" value="{{$backlog->sprint}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <button class="mx-2 float-right btn btn-sm btn-primary">Guardar</button>
                            <a href="{{route('backlog.index')}}" class="mx-2 float-right btn btn-sm btn-secondary">Cancelar</a>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection