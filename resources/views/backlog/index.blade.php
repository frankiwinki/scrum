@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('aside')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Listado Backlog') }} <a href="{{route('backlog.create')}}" class="btn btn-sm btn-success float-right">Crear Historia de Usuario</a></div>

                <div class="card-body">

                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Proyecto</th>
                                <th>Nombre</th>
                                <th>Sprint</th>
                                <th>Eliminar</th>
                                <th>Modificar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($backlog as $item)
                            <tr>
                                <td>{{$item->projects->name}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->sprint}}</td>
                                <td>
                                    
                                    <form action="{{route('backlog.destroy',['backlog'=>$item->id])}}" method="post">
                                        <input class="btn btn-sm btn-danger" type="submit" value="Delete" />
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </td>
                                <td><a href="{{route('backlog.edit',['backlog'=>$item->id])}}" class="btn btn-sm btn-primary">Modificar</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="card-footer">
                    <a href="{{route('home')}}" class="float-right btn btn-sm btn-secondary">Atrás</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection